# Legal One BE Coding Challenge

The solution of this coding Challenge has been developed using the latest version of Laravel (9.X)

## Start local devlopment server

Start Laravel's local development server using the Laravel's Artisan CLI serve command:
```
php artisan serve
```

# Deployment

### Requirements
- PHP 7.4+
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- MySQL 5.7+ (Or MariaDB 10.1+)

After deployment make sure to run these:
```
php artisan key:generate
php artisan config:cache
php artisan route:cache
php artisan migrate:fresh
```

### Configuration

You will find a file named `.env.example` which has configuration variables.
Rename this file to `.env` and change whatever you see fit for your environment.

## Start local devlopment server

Start Laravel's local development server using the Laravel's Artisan CLI serve command:
```
php artisan serve
```

## Tasks

1. To insert the data from logs file into your database, you should use this command : 
```
php artisan insert:data
```

2. The second task can be tested by browsing into the URL http://example.com/api/count which has several filters
   - serviceNames
   - statusCode
   - startDate
   - endDate