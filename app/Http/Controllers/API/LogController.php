<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Log;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function count(Request $request)
    {
        $logs = Log::query();
        if($request->serviceName){
            $logs = $logs->where('service_name',$request->serviceName);
        }
        if($request->statusCode){
            $logs = $logs->where('status_code',$request->statusCode);
        }
        if ($request->startDate) {
            $logs = $logs->where('created_at', '>=', $request->startDate);
        }
        if ($request->endDate) {
            $logs = $logs->where('created_at', '<=', $request->endDate);
        }
        $count = $logs->count();

        return response()->json([
            'counter' => $count
        ]);
    }

}
