<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Log;

class InsertDataFromLogFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $handle = fopen(storage_path('app/logs.txt'), "r");
        $i = 0;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $i++;
                $line = trim($line);
                $serviceNames = trim(explode('- -',$line)[0]);
                $statusCode = trim(explode('"',$line)[2]);
                $createdAt =  Carbon::parse(explode("]",explode('[',$line)[1])[0])->format('Y-m-d H:i:s');

                $log = Log::where('created_at','=',$createdAt)->first();

                if(!$log){
                    Log::create([
                        'service_name' => $serviceNames,
                        'status_code' => $statusCode,
                        'created_at' =>  $createdAt
                    ]);
                }
            }

            fclose($handle);
        }
        echo "File has been insered into database";
    }
}
